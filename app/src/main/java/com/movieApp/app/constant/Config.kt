package com.movieApp.app.constant

/** General configuration */
object Config {
    const val CURRENCY_PREFIX = "Rp "
    const val TIME_INPUT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val DATE_INPUT = "yyyy-MM-dd"
    const val DATE_OUTPUT = "d MMMM yyyy"
    const val TIME_OUTPUT = "d MMMM yyyy, HH:mm"
    const val AUTOCOMPLETE_COUNTRY = "country:id"
    const val LOG = "LOG_DEBUG"
    const val BANNER_SLIDE_TIME: Long = 5000
    const val SUGGESTION_DELAY: Long = 500
    const val SUGGESTION_DELAY_LOCATION: Long = 750
    const val DEFAULT_MAP_ZOOM = 13F
    const val REQUEST_TIMEOUT = 30L
    const val DEFAULT_MAP_LATITUDE = -6.947114
    const val DEFAULT_MAP_LONGITUDE = 107.601397
    const val PERCENTAGE = 100.0
    const val REQUEST_IMAGE: Int = 101
    const val REQUEST_SAVE_FILE: Int = 102
    const val REQUEST_PICK_FILE: Int = 103
    const val DEFAULT_IMAGE_SIZE = 1280
    const val LANGUAGE = "en-US"
}
